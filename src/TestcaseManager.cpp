#include <QHash>

#include "Testcase.h"
#include "TestcaseManager.h"


TestcaseManager *TestcaseManager::m_instance = NULL;


TestcaseManager::TestcaseManager(QObject *parent) :
        QObject(parent),
        m_testcases(QHash<QString, Testcase *>())
{
}


TestcaseManager *TestcaseManager::instance()
{
    if (NULL == m_instance) {
        m_instance = new TestcaseManager();
    }

    return m_instance;
}



bool TestcaseManager::registerTestcase(Testcase *testcase)
{
    m_testcases.insert(testcase->metaObject()->className(), testcase);
    return true;
}


const QHash<QString, Testcase *> TestcaseManager::testcases() const
{
    return m_testcases;
}
