/*!
 * \file
 * \author Eric MSP Veith <eveith@veith-m.de>
 * \date 2013-03-16
 */


#ifndef TESTCASE_H
#define TESTCASE_H


#include <QObject>


class QFile;
class QTextStream;


class Testcase : public QObject
{
    Q_OBJECT

private:


    /*!
     * Target experiment output result file
     */
    QFile *m_resultFile;


    /*!
     * Target result stream
     *
     * \sa #resultStream
     */
    QTextStream *m_resultStream;


    /*!
     * Creates the result stream, possibly safing and closing an old one.
     */
    void createResultStream();


    /*!
     * Closes and saves an existing result stream. Does nothing if there is no
     * such stream.
     */
    void closeResultStream();


protected:


    /*!
     * \brief Provides access to the test result stream
     *
     * \return A `QTextStream` instance pointing to the appropriate file.
     */
    QTextStream &resultStream();


public:


    /*!
     * Is `true` when the testcase has been registered with the manager.
     */
    static bool __registered;


    explicit Testcase(QObject *parent = 0);
    virtual ~Testcase();
};

#endif // TESTCASE_H
