/*!
 * \file
 * \author Eric MSP Veith <eveith@gnyu-linux.org>
 * \date 2013-03-16
 */


#ifndef TESTCASEMANAGER_H
#define TESTCASEMANAGER_H


#include <QObject>
#include <QHash>


class Testcase;


class TestcaseManager : public QObject
{
    Q_OBJECT

private:


    /*!
     * The singleton instance
     */
    static TestcaseManager *m_instance;


    /*!
     * A list of testcases, indexed by their class name for easy lookup
     */
    QHash<QString, Testcase *> m_testcases;


    /*!
     * Private singleton constructor
     */
    explicit TestcaseManager(QObject *parent = 0);


public:


    /*!
     * Get the singleton instance of the manager.
     */
    static TestcaseManager *instance();


    /*!
     * Registers a testcase.
     */
    bool registerTestcase(Testcase *testcase);


    /*!
     * \return The testcase hash
     */
    const QHash<QString, Testcase *> testcases() const;
};


/*!
 * This macro registeres a testcase class with the testcase manager. Use this
 * macro once in the source file where the method bodies of your testcase
 * reside.
 */
#define REGISTER_TESTCASE(testcase) \
     bool __##testcase##_registered =\
        TestcaseManager::instance()->registerTestcase(new testcase())

#endif // TESTCASEMANAGER_H
