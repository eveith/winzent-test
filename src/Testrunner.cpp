#include <cstdlib>
#include <iostream>

#include <QCoreApplication>
#include <QtTest>

#include <QList>
#include <QHash>

#include <QFile>
#include <QIODevice>
#include <QDebug>

#include "TestcaseManager.h"
#include "Testcase.h"

#include "Testrunner.h"

using std::cout;
using std::cerr;
using std::endl;


Testrunner::Testrunner()
{
}


void Testrunner::printHelp(const char *argv0)
{
    cout
            << endl
            << argv0 << " -- A experiments collection/test runner"
            << endl
            << "Usage: winzent-experiments [options] "
               << "[testcase] [testcase options]"
            << endl
            << endl
            << "Without any parameters or arguments, all testcases are run."
            << endl
            << "Options:"
            << endl
            << " -h      Shows this help screen"
            << endl
            << " -l      Prints a list of all known testcases"
            << endl
            << endl
            << "Additional arguments to testcases are also accepted"
            << endl
            << "and handled by Qt's testcase library."
            << endl << endl;
}


void Testrunner::printTestcases()
{
    foreach (QString i, TestcaseManager::instance()->testcases().keys()) {
        cout << i.toStdString() << endl;
    }
}


int Testrunner::runtests(int argc, char *argv[])
{
    TestcaseManager *testcaseManager = TestcaseManager::instance();
    QList<Testcase *> testPlan;
    QStringList testcaseArguments;

    {
        bool seenOwnArgs = false;
        for (int i = 1; i != argc; ++i) {
            if (! seenOwnArgs && '-' == argv[i][0] && '\0' != argv[i][1]) {
                switch (argv[i][1]) {
                    case 'h':
                        Testrunner::printHelp(argv[0]);
                        return EXIT_SUCCESS;
                        break;
                    case 'l':
                        Testrunner::printTestcases();
                        return EXIT_SUCCESS;
                        break;
                    case '-':
                        // Arguments for test cases follow; finished.
                        seenOwnArgs = true;
                        break;
                }
            } else {
                seenOwnArgs = true;

                if (testcaseManager->testcases().contains(QString(argv[i]))) {
                    testPlan << testcaseManager->testcases()[QString(argv[i])];
                } else {
                    testcaseArguments << QString(argv[i]);
                }
            }
        }
    }

    if (0 == testPlan.size()) {
        testPlan = TestcaseManager::instance()->testcases().values();
    }

    cout << "Test plan:" << endl;
    foreach (Testcase *i, testPlan) {
        cout << "  * " << i->metaObject()->className() << endl;
    }

    int rc = EXIT_SUCCESS;
    QCoreApplication app(argc, argv);

    foreach (Testcase *i, testPlan) {
        rc |= QTest::qExec(i, QStringList(testcaseArguments));
    }

    return rc;
}
