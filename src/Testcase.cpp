#include <QObject>
#include <QtTest>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QDateTime>

#include "Testcase.h"


Testcase::Testcase(QObject *parent):
        QObject(parent),
        m_resultFile(0),
        m_resultStream(0)
{
}


Testcase::~Testcase()
{
    closeResultStream();
}


void Testcase::createResultStream()
{
    closeResultStream();

    QDir()
            .mkpath((QString("./testlog-%1")
            .arg(this->metaObject()->className())));

    m_resultFile = new QFile(QString("./testlog-%1/%2-%3.out")
            .arg(this->metaObject()->className())
            .arg(QTest::currentTestFunction())
            .arg(QDateTime::currentDateTime().toString(Qt::ISODate)));
    m_resultFile->open(QIODevice::Text
            | QIODevice::WriteOnly | QIODevice::Truncate);

    m_resultStream = new QTextStream(m_resultFile);
}


void Testcase::closeResultStream()
{
    if (NULL != m_resultStream) {
        m_resultStream->flush();
        delete m_resultStream;
    }

    if (NULL != m_resultFile) {
        m_resultFile->flush();
        m_resultFile->close();
        delete m_resultFile;
    }

    m_resultFile = 0;
    m_resultStream = 0;
}


QTextStream &Testcase::resultStream()
{
    if (0 == m_resultStream) {
        createResultStream();
    }

    return *m_resultStream;
}
