QT += testlib
QT -= gui

TARGET = winzent-test
TEMPLATE = lib

DEFINES += WINZENTTEST_LIBRARY

HEADERS += \
        winzent-test_global.h \
        Testcase.h \
        TestcaseManager.h \
    Testrunner.h

SOURCES += \
        Testcase.cpp \
        TestcaseManager.cpp \
        Testrunner.cpp

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
