#!/bin/sh
# testrunner.sh -- Winzent test runner for LD_LIBRARY_PATH
# Copyright (C) 2013  Eric MSP Veith <eveith@veith-m.de>


set -e


opts=`getopt -o 'l:' -- "$@"`
eval set -- "$opts"


while true; do
    case "$1" in
        '--')
            break;;
        '-l')
            shift
            LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${1}"
            shift;;
        *)
            echo >&2 "Usage: ${0} -l PATH [-l PATH ...] binary"
            exit 1;;
    esac
done

export LD_LIBRARY_PATH
exec $@
