#ifndef TESTRUNNER_H
#define TESTRUNNER_H


class Testrunner
{
private:


    Testrunner();


public:


    /*!
     * \brief Prints a help text to STDOUT
     */
    static void printHelp(const char *argv0);


    /*!
     * \brief Prints a list of all known testcases to STDOUT
     */
    static void printTestcases();


    /*!
     * \brief Takes all command line arguments and manages testcase execution
     *  and parameter handling.
     *
     * \param argc Argument count
     *
     * \param argv Argument values
     */
    static int runtests(int argc, char *argv[]);
};


/*!
 * \brief Expands to a simple main() function that invokes
 *  Testrunner::runtests().
 */
#define TESTRUNNER_MAIN \
    int main(int argc, char *argv[]) \
    { \
        return Testrunner::runtests(argv, argv); \
    } \


#endif // TESTRUNNER_H
